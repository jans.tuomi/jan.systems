FROM node AS builder
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
COPY src src
RUN npm run build

FROM fnichol/uhttpd AS server
COPY --from=builder /app/dist /www
